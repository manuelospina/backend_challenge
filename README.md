# Back End Developer Exercise

## Challenge


### (Required) Create a database schema for the application.


### Write code for reading and validating the clients csv files into the database.

These can be done through the CLI:

> python manage.py load_shifts files/shifts.csv 

and

> python manage.py load_employees files/employees.csv

There is also a endpoint in both employees and shifts to upload the CSV file:

```
/v1/shifts/loadfile/

Payload:
  { datafile: data }
```

and

```
/v1/employees/loadfile/

Payload:
  { datafile: data }
```


### Design and/or implement a web API.

The main endpoints are

- /v1/employees/       (get, post)
- /v1/employees/{pik}  (get, patch, put, delete)
- /v1/shifts           (get, post)
- /v1/shifts/{pik}     (get, patch, put, delete)
- /v1/locations/       (get, post)
- /v1/locations/{pil}  (get, patch, put , delete)
- /v1/roster/{location_id}/shifts (get, post)
  - Get or update a list of shifts of a given location. 
  - It accepts start_date and end date to filter the period         
- /v1/roster/{location_id}/employees (get)
  - Return a list of employees for a given location

### Develop some questions (for the rosterer) 

- Are employees assign to one or multiple locations?
- Who can assign employees? 
- Who can add employees and shifts?
- How often are the shifts assigned? Are they assigned on a monthly basis?

### Design and/or implement a pattern for calling an external process 

TO DO

## Design and/or implement a pattern for validating shifts and returning or storing warnings

TO DO
