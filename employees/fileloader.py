import csv

from employees.models import Employee

# File -> Boolean
def load_data_from_file(file_path):
    """
    Save the data from a csv file in the RDB and return true on success
    """
    with open(file_path, newline='') as fh:
        load_csv_data(fh)
    fh.close()

    return True

def load_csv_data(filehandler):
    csv_data = csv.reader(filehandler)
    _headers = next(csv_data)
    employees = map(
        lambda row: Employee(first_name=row[0], last_name=row[1]),
        csv_data
    )
    Employee.objects.bulk_create(employees)
