from django.db import models
from locations.models import Location

class Employee(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    # relationships
    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        null=True, 
        related_name='employees',
        related_query_name='employee'
    )

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
