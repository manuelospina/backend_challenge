from factory.django import DjangoModelFactory
from employees.models import Employee

class EmployeeFactory(DjangoModelFactory):
    class Meta:
        model = Employee

    first_name = 'New'
    last_name = 'Employee'

