from io import StringIO
from django.core.management import call_command
from django.test import TestCase
from employees.models import Employee

class LoadEmployeesTest(TestCase):

    def setUp(self):
        self.employees_file = 'files/employees.csv'
        self.command = 'load_employees'

    def test_save_into_rdb(self):
        out = StringIO()
        before_count = Employee.objects.count()
        call_command(self.command, self.employees_file, stdout=out)
        after_count = Employee.objects.count()

        self.assertGreater(after_count, before_count)
