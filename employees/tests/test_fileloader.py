import unittest
from employees import fileloader
from employees.models import Employee

class TestFileLoader(unittest.TestCase):

    def test_load_data_from_file(self):
        before_count = Employee.objects.count()
        result = fileloader.load_data_from_file('files/employees.csv')
        after_count = Employee.objects.count()

        self.assertTrue(result)
        self.assertGreater(after_count, before_count)

    def test_load_csv_data(self):
        before_count = Employee.objects.count()
        with open('files/employees.csv') as fh:
            fileloader.load_csv_data(fh)
        after_count = Employee.objects.count()

        self.assertGreater(after_count, before_count)
