from django.test import TestCase
from employees.models import Employee
from locations.models import Location

class EmployeeModelTest(TestCase):
    
    def setUp(self):
        self.new_employee = Employee(first_name='New', last_name='Employee')

    def test_save(self):
        initial_count = Employee.objects.count()
        self.new_employee.save()
        new_count = Employee.objects.count()
        self.assertNotEqual(initial_count, new_count)

    def test_association_to_location(self):
        self.new_employee.location = Location.objects.create(name='Location')
        self.new_employee.save()
        employee = Employee.objects.get(location__name='Location')
        self.assertEqual(self.new_employee, employee)
