from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status

from employees.models import Employee

class EmployeeListTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.base_url = '/v1/employees/'
        self.payload = {'first_name': 'New', 'last_name': 'Employee'}

    def test_employee_get(self):
        response = self.client.get(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_employee_post(self):
        response = self.client.post(self.base_url, self.payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response['content-type'], 'application/json')

class EmployeeDetailTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.employee = Employee.objects.create()
        self.base_url = '/v1/employees/{}'.format(self.employee.id)

    def test_employee_get(self):
        response = self.client.get(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_employee_put(self):
        response = self.client.put(
            self.base_url,
            {'first_name': 'New', 'last_name': 'Employee'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_employee_delete(self):
        response = self.client.delete(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class EmployeeLoadFileTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.base_url = '/v1/employees/loadfile/'

    def test_loadfile_post(self):
        with open('files/employees.csv') as fh:
            response = self.client.post(
                self.base_url,
                {'datafile': fh.read()},
                format='multipart'
            )
            self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        count = Employee.objects.count()
        self.assertGreater(count, 0)
