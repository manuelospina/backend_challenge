from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser

from io import StringIO

from employees.models import Employee
from employees.serializers import EmployeeSerializer
from employees import fileloader

class EmployeeList(generics.ListCreateAPIView):
    """
    get: 
    Return a list of all the employees.

    post:
    Create a new employee instance.
    """

    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    get:
    Return the given employee information.

    patch:
    Update a field in the given employee record.

    delete:
    Delete the given employee record.
    """

    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeLoadFile(APIView):
    """
    post:
    Create employee instances from the given CSV file.
    """

    parser_classes = (MultiPartParser,)

    def post(self, request, format=None):
        datafile = self.request.data.get('datafile')
        fileloader.load_csv_data(StringIO(datafile))
        return Response(status=status.HTTP_204_NO_CONTENT)
