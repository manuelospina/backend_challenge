from django.db import models

class Location(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{}".format(self.name)
