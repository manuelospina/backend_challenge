from factory.django import DjangoModelFactory
from locations.models import Location

class LocationFactory(DjangoModelFactory):
    class Meta:
        model = Location

    name = 'Location'
