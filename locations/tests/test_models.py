from django.test import TestCase
from locations.models import Location

class LocationModelTest(TestCase):
    
    def setUp(self):
        self.new_location = Location(name='Location')

    def test_save(self):
        initial_count = Location.objects.count()
        self.new_location.save()
        new_count = Location.objects.count()
        self.assertNotEqual(initial_count, new_count)
