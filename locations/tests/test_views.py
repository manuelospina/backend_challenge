from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from locations.models import Location

class LocationListTest(TestCase):
    
    def setUp(self):
        self.client = APIClient()
        self.base_url = '/v1/locations/'
        self.payload = {'name': 'Location'}

    def test_location_get(self):
        response = self.client.get(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_location_post(self):
        response = self.client.post(self.base_url, self.payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response['content-type'], 'application/json')

class LocationDetailTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.location = Location.objects.create(name='Location')
        self.base_url = '/v1/locations/{}'.format(self.location.id)

    def test_location_get(self):
        response = self.client.get(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_location_patch(self):
        response = self.client.patch(
            self.base_url,
            {'name': 'Name'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_location_delete(self):
        response = self.client.delete(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
