from rest_framework import generics
from locations.models import Location
from locations.serializers import LocationSerializer

class LocationList(generics.ListCreateAPIView):
    """
    get:
    Return a list of all locations.

    post:
    Create a new location instance.
    """
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

class LocationDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    get:
    Return the details for the given location.

    patch:
    Update one or more fields of the given location.

    delete:
    Destroy the record of the given location.
    """
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
