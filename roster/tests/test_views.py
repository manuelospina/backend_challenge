from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status

from locations.tests.factories import LocationFactory
from shifts.tests.factories import ShiftFactory
from shifts.models import Shift
from employees.tests.factories import EmployeeFactory

class LocationShiftViewTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.location = LocationFactory()
        self.shift = ShiftFactory(location=self.location)
        self.employee = EmployeeFactory(location=self.location)
        self.base_url = '/v1/roster/{}/shifts'.format(self.location.id)
        self.query = {
            'start_date': '2018-06-10',
            'end_date': '2018-07-09',
            }
        self.payload = {
            'shifts': [{
                "id": self.shift.id,
                'employee': self.employee.id
            }]
        }

    def test_roster_get(self):
        ShiftFactory() # wrong location
        ShiftFactory(date='2018-06-09', location=self.location) # Wrong date

        response = self.client.get(self.base_url, self.query)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')
        self.assertEqual(len(response.data), 1)

    def test_roster_patch(self):
        response = self.client.patch(self.base_url, self.payload)
        shift = Shift.objects.get(pk=self.shift.id)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')
        self.assertEqual(shift.employee, self.employee)

class LocationEmployeeViewTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.location = LocationFactory()
        self.employee = EmployeeFactory(location=self.location)
        self.base_url = '/v1/roster/{}/employees'.format(self.location.id)

    def test_get(self):
        EmployeeFactory() # wrong location

        response = self.client.get(self.base_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')
        self.assertEqual(len(response.data), 1)
