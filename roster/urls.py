from django.urls import path
from . import views

urlpatterns = [
    path('<int:location_id>/shifts', views.LocationShiftView.as_view()),
    path('<int:location_id>/employees', views.LocationEmployeeView.as_view()),
]
