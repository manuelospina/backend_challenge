import datetime

from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser

from shifts.models import Shift
from shifts.serializers import ShiftSerializer
from employees.models import Employee
from employees.serializers import EmployeeSerializer

class LocationShiftView(APIView):
    """
    get:
    Return a list of all the shifts for a given location.
    It accepted a start_date and end_date parameters to filter
    the date range of the shifts.

    patch:
    Update the information for the given shifts.
    payload:
      {
        'shifts': [{
          "id": Int,
          'employee': Int
        }]
      }

    """
    def get(self, request, location_id):
        start_date = request.GET.get('start_date', datetime.date.min)
        end_date = request.GET.get('end_date', timezone.now().date())

        shifts = Shift.objects.filter(
            location__id=location_id,
            date__range=[start_date, end_date]
        )

        serializer = ShiftSerializer(shifts, many=True)
        return Response(serializer.data)

    def patch(self, request, location_id):
        data = request.data.getlist('shifts')
        shifts = [self._batch_update(s) for s in data]
        serializer = ShiftSerializer([s for s in shifts if s], many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def _batch_update(self, shift):
        import json
        shift = json.loads(shift.replace("'", '"'))

        try:
            shift_obj = Shift.objects.get(pk=shift.get('id', ''))
        except ObjectDoesNotExist:
            return None # Log error
        else:
            shift_obj.employee_id = shift.get('employee', '')
            shift_obj.save()
            return shift_obj
        
class LocationEmployeeView(APIView):
    """
    get:
    Return a list of employees for the given location
    """

    def get(self, request, location_id):
        employees = Employee.objects.filter(location__id=location_id)

        serializer = EmployeeSerializer(employees, many=True)
        return Response(serializer.data)
