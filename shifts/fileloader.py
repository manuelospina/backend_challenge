import csv
from datetime import datetime, timedelta

from shifts.models import Shift

# File -> Boolean
def load_data_from_file(file_path):
    """
    Save the data from a csv file in the RDB and return true on success
    """
    with open(file_path, newline='') as fh:
        load_csv_data(fh)
    fh.close()

    return True

def load_csv_data(filehandler):
    csv_data = csv.reader(filehandler)
    _headers = next(csv_data)
    shifts = [build_shift(row) for row in csv_data if row]
    Shift.objects.bulk_create(shifts)

def build_shift(row):
    return Shift(
        date=datetime.strptime(row[0], '%d/%m/%Y').date().isoformat(),
        start=datetime.strptime(row[1], '%I:%M:%S %p').time().isoformat(),
        end=datetime.strptime(row[2], '%I:%M:%S %p').time().isoformat(),
        rest=timedelta(minutes=int(row[3]))
    )
