from django.core.management.base import BaseCommand, CommandError
from shifts import fileloader

class Command(BaseCommand):
    help = 'Save data from the given CSV file to the RDB'

    def add_arguments(self, parser):
        parser.add_argument('filenames', nargs='+', type=str)

    def handle(self, *args, **options):
        for filename in options['filenames']:
            fileloader.load_data_from_file(filename)
