from django.db import models

from employees.models import Employee
from locations.models import Location

from .validations import ValidationMixin

class Shift(models.Model, ValidationMixin):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)
    date = models.DateField()
    start = models.TimeField()
    end = models.TimeField()
    rest = models.DurationField()
    # relationships
    employee = models.ForeignKey(
        Employee,
        on_delete=models.CASCADE,
        null=True, 
        related_name='shifts',
        related_query_name='shift'
    )
    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        null=True, 
        related_name='shifts',
        related_query_name='shift'
    )
    
    def clean(self):
        self.same_time(self)

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    def __str__(self):
        return "{} {}-{} {}".format(
            self.date,
            self.start,
            self.end,
            self.rest
        )
