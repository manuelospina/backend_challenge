from rest_framework import serializers
from shifts.models import Shift

class ShiftSerializer(serializers.ModelSerializer):

    date = serializers.DateField(input_formats=['iso-8601', '%d/%M/%Y'])
    
    class Meta:
        model = Shift
        fields = ('id', 'date', 'start', 'end', 'rest', 'employee', 'location')
