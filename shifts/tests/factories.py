from factory.django import DjangoModelFactory
from shifts.models import Shift

class ShiftFactory(DjangoModelFactory):
    class Meta:
        model = Shift

    date = '2018-07-01'
    start = '09:00:00'
    end = '15:00:00'
    rest = '60'
