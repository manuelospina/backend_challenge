from io import StringIO
from django.core.management import call_command
from django.test import TestCase
from shifts.models import Shift

class LoadShiftsTest(TestCase):

    def setUp(self):
        self.shifts_file = 'files/shifts.csv'
        self.command = 'load_shifts'

    def test_save_into_rdb(self):
        out = StringIO()
        before_count = Shift.objects.count()
        call_command(self.command, self.shifts_file, stdout=out)
        after_count = Shift.objects.count()

        self.assertGreater(after_count, before_count)
