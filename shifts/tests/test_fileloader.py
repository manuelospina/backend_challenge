import unittest
from shifts import fileloader
from shifts.models import Shift

class TestFileLoader(unittest.TestCase):

    def test_load_data_from_file(self):
        before_count = Shift.objects.count()
        result = fileloader.load_data_from_file('files/shifts.csv')
        after_count = Shift.objects.count()
        self.assertTrue(result)
        self.assertGreater(after_count, before_count)

    def test_load_csv_data(self):
        before_count = Shift.objects.count()
        with open('files/shifts.csv') as fh:
            fileloader.load_csv_data(fh)
        after_count = Shift.objects.count()

        self.assertGreater(after_count, before_count)
