from django.test import TestCase
from shifts.models import Shift
from employees.models import Employee
from locations.models import Location

class ShiftModelTest(TestCase):
    
    def setUp(self):
        self.new_shift = Shift(
            date='2018-06-18',
            start='5:00:00 AM',
            end='1:30:00 PM',
            rest='60'
        )

    def test_save(self):
        initial_count = Shift.objects.count()
        self.new_shift.save()
        new_count = Shift.objects.count()
        self.assertNotEqual(initial_count, new_count)

    def test_association_to_employee(self):
        self.new_shift.employee = Employee.objects.create(first_name='New', last_name='Employee')
        self.new_shift.save()
        shift = Shift.objects.get(employee__first_name='New')
        self.assertEqual(self.new_shift, shift)

    def test_association_to_location(self):
        self.new_shift.location = Location.objects.create(name='Location')
        self.new_shift.save()
        shift = Shift.objects.get(location__name='Location')
        self.assertEqual(self.new_shift, shift)
