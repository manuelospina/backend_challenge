from django.test import TestCase
from django.core.exceptions import ValidationError

from shifts.models import Shift
from employees.models import Employee
from shifts.tests.factories import ShiftFactory
from employees.tests.factories import EmployeeFactory

class ShiftModelValidationsTest(TestCase):
    
    def setUp(self):
        self.employee = EmployeeFactory()
        self.shift = ShiftFactory(employee=self.employee)

    def test_same_time(self):
        """
        Employee can't be assigned to the same shift
        """
        new_shift = ShiftFactory()
        with self.assertRaises(ValidationError) as cm:
            new_shift.employee = self.employee
            new_shift.save()
        
        self.assertIn('already assigned', cm.exception.message)

    def test_back_to_back(self):
        """
        Employee can't be assigned to two continuos shifts
        """
        early_shift = ShiftFactory(start='03:00:00', end='09:00:00')
        with self.assertRaises(ValidationError) as cm:
            early_shift.employee = self.employee
            early_shift.save()

        self.assertIn('already assigned', cm.exception.message)

    def test_five_days_in_a_row(self):
        """
        An employee can only work 5 days in a row
        """
        pass

    def test_five_out_of_seven(self):
        """
        An employee can only work 5 days out of seven
        """
        pass

    def test_overnight_rest(self):
        """
        An employee has a minimum of 10 hours overnight rest
        """
        pass
