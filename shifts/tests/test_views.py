from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from shifts.models import Shift
from employees.models import Employee
from locations.models import Location

class ShiftListTest(TestCase):
    
    def setUp(self):
        self.client = APIClient()
        self.base_url = '/v1/shifts/'
        self.payload = {'date': '18/06/2018',
                        'start': '5:00:00 AM',
                        'end': '1:30:00 PM',
                        'rest': '60'}

    def test_shift_get(self):
        response = self.client.get(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_shift_post(self):
        response = self.client.post(self.base_url, self.payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response['content-type'], 'application/json')

class ShiftDetailTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.shift = Shift.objects.create(
            date='2018-06-18',
            start='5:00:00 AM',
            end='1:30:00 PM',
            rest='60')
        self.base_url = '/v1/shifts/{}'.format(self.shift.id)

    def test_shift_get(self):
        response = self.client.get(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_shift_patch(self):
        employee = Employee.objects.create(first_name='New', last_name='Employee')
        locations = Location(name='Location')
        response = self.client.patch(
            self.base_url,
            {'employee': employee.id}, # This test do nothing
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['content-type'], 'application/json')

    def test_shift_delete(self):
        response = self.client.delete(self.base_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class ShiftLoadFileTest(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.base_url = '/v1/shifts/loadfile/'

    def test_loadfile_post(self):
        with open('files/shifts.csv') as fh:
            response = self.client.post(
                self.base_url,
                {'datafile': fh.read()},
                format='multipart'
            )
            self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        count = Shift.objects.count()
        self.assertGreater(count, 0)
