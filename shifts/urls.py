from django.urls import path
from shifts import views

urlpatterns = [
    path('', views.ShiftList.as_view()),
    path('<int:pk>', views.ShiftDetail.as_view()),
    path('loadfile/', views.ShiftLoadFile.as_view()),
]
