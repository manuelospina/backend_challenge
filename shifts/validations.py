from datetime import timedelta

from django.core.exceptions import ValidationError


class ValidationMixin:

    @classmethod
    def same_time(cls, shift): 
        """
        shift -> Boolean
        Raise an error if the employee has been assigned to the same shift
        """
        if not shift.employee:
            return

        shifts = cls.objects.filter(
            date=shift.date,
            start__range=[shift.start, shift.end],
            end__range=[shift.start, shift.end],
            employee=shift.employee
        )
        if len(shifts): 
            raise ValidationError(
                "Employee {} is already assigned.".format(shift.employee),
                params={'employee': shift.employee}
                )

    @classmethod
    def back_to_back(cls, shift):
        pass
