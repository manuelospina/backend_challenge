from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser

from io import StringIO

from shifts.models import Shift
from shifts.serializers import ShiftSerializer
from shifts import fileloader

class ShiftList(generics.ListCreateAPIView):
    """
    get:
    Return list of all shifts.

    post:
    Create a new shift instance.
    """

    queryset = Shift.objects.all()
    serializer_class = ShiftSerializer

class ShiftDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    get:
    Return details of the given shift.

    patch: 
    Update one or more fields for the given shift.

    delete:
    Destroy the given shift record.
    """

    queryset = Shift.objects.all()
    serializer_class = ShiftSerializer

class ShiftLoadFile(APIView):
    """
    post:
    Create shifts instances from a given CSV file.
    """

    parser_classes = (MultiPartParser,)

    def post(self, request, format=None):
        datafile = self.request.data.get('datafile')
        fileloader.load_csv_data(StringIO(datafile))
        return Response(status=status.HTTP_204_NO_CONTENT)

